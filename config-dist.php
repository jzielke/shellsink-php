<?php

/*
 * Web path, leave blank if running from root domain.
 * https://example.com/sink/ would use $path = 'sink';
 * https://history.example.com/ would use $path = '';
 */
$path = '';

/*
 * $enable_registration is used to control new user creation.
 * Setting this to 1 will allow creating new users.
 * Setting this to 0 will prevent creating new users.
 */
$enable_registration = 1;

/*
 * Google OAuth 2.0
 * https://developers.google.com/accounts/docs/OpenIDConnect
 *
 * Example redirect URIs:
 * https://example.com/sink/auth
 * https://history.example.com/auth
 * Add /auth to the end of your shellsink URL.
 *
 * Use Client ID for $GoogleId
 * Use Client secret for $GoogleSecret
 */
$GoogleId = 'SOME-LONG-STRING.apps.googleusercontent.com';
$GoogleSecret = 'RANDOMCHARACTERS';

/*
 * $set_login_cookie is used to set a 'remember me' cookie in the browser.
 * Setting this to 1 will set a cookie to bypass OAuth authentication on return
 * visits.
 * If set to 0 the next 3 variables do not need to be set.
 *
 * $cookie_age sets the age the cookie remains valid for in days.  This needs
 * to be set somewhat high or a stolen session will not be caught.
 * $remembermekey needs to be long and use only hexidecimal characters.
 *
 * $cleanupkey is used to authenicate a cleanup script.  A few random characters
 * sent to base64 should be fine.
 * @monthly or @yearly:
 * curl -qd auth=RANDOMCHARACTERS https://example.com/sink/cleanup
 */
$set_login_cookie = 0;
//$cookie_age = 183;   // 6 months
//$remembermekey = pack('H*', 'longlonglonghexkey');
//$cleanupkey = 'RANDOMCHARACTERS';

/*
 * Setting $debug to 1 will cause some messages to be written to
 * $logdir/debug.log
 */
$debug = 0;
$logdir = '/path/to/debug/folder';

/*
 * git clone https://github.com/google/google-api-php-client
 * Set $googleapi to google-api-php-client/src
 */
$googleapi = '/path/to/google-api-php-client/src';

/*
 * $commands_per_page sets the number of commands per page on the history and
 * search pages.  It somewhat non-obviously also sets the number of commands
 * sent to the client during a pull command.
 */
$commands_per_page = 20;

/*
 * $max_filters sets the maximum number of filters for the atom feed.  If
 * you change this make sure you also change shellsink.js to match.
 */
$max_filters = 5;	// Needs to match web/js/shellsink.js

/*
 * $max_user_ids sets the maximum number of unique IDs one login can have.
 */
$max_user_ids = 5;

/*
 * If you use tracking software you can put the HTML here.  $tracking is
 * displayed in the footer.
 */
$tracking = '
';

/*
 * This allows the auth page to find the Google API scripts.  It should not need
 * to be edited.  Set $googleapi above.
 */
set_include_path(get_include_path() . PATH_SEPARATOR . $googleapi);

/*
 * Set host, port, user, pass, and name as needed.
 */
function connect_sql() {
  global $db;
  $db_type = 'mysql';
  $db_host = '127.0.0.1';
  $db_port = 3306;
  $db_user = 'shellsink_user';
  $db_pass = 'xxxxxxxx';
  $db_name = 'shellsink';
  try {
    $db = new PDO("$db_type:host=$db_host;port=$db_port;dbname=$db_name", $db_user, $db_pass);
    $db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
    $db->setAttribute(PDO::ATTR_EMULATE_PREPARES, false);
  } catch(PDOException $e) {
    fail('MySQL connect', $e->getMessage());
  }
}

function fail($pub, $pvt = '') {
  global $debug;
  if ($debug) debug_msg('fail: ' . $pub . ' ' . $pvt);

  die();
}

function debug_msg($msg) {
  global $logdir;
  error_log(date('c') . ' ' . $msg . "\n", 3, $logdir . '/debug.log');
}
