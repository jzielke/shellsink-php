# Shellsink PHP #

Shellsink is a web accessible copy of your bash history. This is rewrite of the backend in PHP.

The original App Engine version let you:

 * Browse through your old commands sorted by date
 * Filter commands by tag
 * Search command line contents
 * View Atom feed
 * Filter Atom feed
 * Tag commands
 * Annotate commands
 * Pull old commands into current history file

My PHP version adds:

 * PHP / MySQL backend for wider hosting options
 * Command and tags sent in 1 POST request instead of multiple GET requests
 * Store return value of command
 * Logarithmic pagination in addition to next / previous page navigation
 * Automatic tagging of CWD
 * Automatic tagging of ssh client IP
 * Filter Atom feed by multiple tags
 * URL builder for multiple Atom feeds
 * Multiple IDs under 1 login

### How to install ###
Copy config-dist.php to config.php and edit as needed.  Create the database using schema/shellsink.sql.  Copy code to web server.  Configure web server to serve the files in the web folder.  I suggest copying all of the code to a folder outside of the web root and then using a symbolic link to the web folder.  You will need [Google OAuth 2.0 credentials](https://developers.google.com/identity/protocols/OpenIDConnect) and the [Google API PHP client](https://github.com/google/google-api-php-client) for authentication.

### How to use it ###
Login with Google to create an account. You will be taken to the preferences page where you will see your secret ID string. Install shellsink-client in your path and add the displayed code to your .bashrc. After you run some commands you should find a searchable index of them on the history page. Commands are automatically tagged by folder, hostname and tty. 

### Maintenance ###
Over time you may accumulate expired authentication tokens.  Set $cleanupkey to some random characters in config.php and add the following to your crontab.  Replace RANDOMCHARACTERS with the characters you set $cleanupkey to and DOMAIN with your domain.

```
#!bash

@yearly curl -qd auth=RANDOMCHARACTERS https://DOMAIN/cleanup
```

### Links to original App Engine version ###
[blogspot.com](http://shell-sink.blogspot.com/#8439280846006649317)  
[Launchpad](https://launchpad.net/shellsink)  
[GitHub](https://github.com/joshuacronemeyer/shellsink)