-- phpMyAdmin SQL Dump
-- version 3.3.2deb1ubuntu1
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Jun 16, 2015 at 12:03 AM
-- Server version: 5.1.73
-- PHP Version: 5.3.10-1ubuntu2ppa6~lucid

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `shellsink`
--

-- --------------------------------------------------------

--
-- Table structure for table `commands`
--

CREATE TABLE IF NOT EXISTS `commands` (
  `command_id` binary(16) NOT NULL,
  `user_id` binary(16) NOT NULL,
  `date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `command` varchar(500) NOT NULL,
  `status` tinyint(4) DEFAULT NULL,
  `annotation` varchar(500) DEFAULT NULL,
  PRIMARY KEY (`command_id`),
  KEY `id` (`command_id`,`user_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `identities`
--

CREATE TABLE IF NOT EXISTS `identities` (
  `user_id` binary(16) NOT NULL,
  `created` timestamp NULL DEFAULT '0000-00-00 00:00:00',
  `alt_id` binary(16) NOT NULL,
  `active` tinyint(1) DEFAULT NULL,
  `name` varchar(500) DEFAULT NULL,
  `filter` varchar(500) DEFAULT NULL,
  UNIQUE KEY `alt_id` (`alt_id`),
  KEY `user_id` (`user_id`,`active`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `tagmap`
--

CREATE TABLE IF NOT EXISTS `tagmap` (
  `command_id` binary(16) NOT NULL,
  `tag_id` bigint(20) NOT NULL,
  KEY `command` (`command_id`,`tag_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `tags`
--

CREATE TABLE IF NOT EXISTS `tags` (
  `tag_id` bigint(20) NOT NULL AUTO_INCREMENT,
  `tag` varchar(500) NOT NULL,
  PRIMARY KEY (`tag_id`),
  UNIQUE KEY `tag` (`tag`(333))
) ENGINE=MyISAM  DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `tokens`
--

CREATE TABLE IF NOT EXISTS `tokens` (
  `user_id` binary(16) NOT NULL,
  `serial` binary(20) NOT NULL,
  `token` binary(20) NOT NULL,
  `expires` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`serial`),
  KEY `user_id` (`user_id`),
  KEY `expires` (`expires`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE IF NOT EXISTS `users` (
  `user_id` binary(16) NOT NULL,
  `created` timestamp NULL DEFAULT '0000-00-00 00:00:00',
  `updated` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00' ON UPDATE CURRENT_TIMESTAMP,
  `name` varchar(500) DEFAULT NULL,
  `email` varchar(500) DEFAULT NULL,
  `url` varchar(500) NOT NULL,
  `atom` tinyint(1) NOT NULL DEFAULT '0',
  `filter` varchar(500) DEFAULT NULL,
  KEY `id` (`user_id`),
  KEY `url` (`url`(333))
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
