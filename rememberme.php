<?php

/*
 * rememberme.php
 * By: Jack Zielke, 2015
 *
 * Initial guide: http://jaspan.com/improved_persistent_login_cookie_best_practice
 * hash_equals function and hmac addition from: http://stackoverflow.com/questions/1354999/keep-me-logged-in-the-best-approach#adzerk1513609780
 *
 * Set $set_login_cookie = 1 to enable rememberme cookie.
 * $db needs to be a PDO instance.
 * $cookie_age is the expiration in days for the authentication cookie.  This needs to be somewhat large.
 * $remembermekey is the binary key used for hmac.
 * $cleanupkey is used to authenticate for cleaning up old keys.
 * $_SESSION['id'] = user ID (auto_increment, md5sum, UUID, etc), current code expects hex ID.
 * function fail($messge) needs to exit() at the very least.
 *
 * $set_login_cookie = 1;	// enabled
 * $cookie_age = 183;		// 6 months
 * $remembermekey = pack('H*', 'hexcharacters');	// output from: hexdump -ve '/1 "%02x"' -n128 /dev/urandom
 * $cleanupkey = 'RANDOMCHARACTERS';			// output from: head -c 30 /dev/urandom | base64
 *
 * 2 comments below reference shellsink.  Remove those lines and use the commented line above them for generic use.
 *
 * @monthly or @yearly cron job that calls cleanup_tokens()
 */

if (!function_exists('hash_equals')) {
  function hash_equals($safe, $user) {
    // Prevent issues if string length is 0
    $safe .= chr(0);
    $user .= chr(0);

    $safeLen = strlen($safe);
    $userLen = strlen($user);

    // Set the result to the difference between the lengths
    $result = $safeLen - $userLen;

    // Note that we ALWAYS iterate over the user-supplied length
    // This is to prevent leaking length information
    for ($i = 0; $i < $userLen; $i++) {
      // Using % here is a trick to prevent notices
      // It's safe, since if the lengths are different
      // $result is already non-0
      $result |= (ord($safe[$i % $safeLen]) ^ ord($user[$i]));
    }

    // They are only identical strings if $result is exactly 0...
    return $result === 0;
  }
}

function check_rememberme_cookie() {
  global $set_login_cookie, $remembermekey, $cookie_age;

  if ($set_login_cookie != 1) return false;

  // returning user?
  $cookie = isset($_COOKIE['rememberme']) ? $_COOKIE['rememberme'] : '';
  if ($cookie) {
    list ($user, $serial, $mac) = explode(':', $cookie);
    $token = get_token($user, $serial);
    if (!$token) return false;
    if (!hash_equals(hash_hmac('sha256', $token, $remembermekey), $mac)) {
      // valid serial, invalid token - erase all keys and warn user
      delete_tokens($user);
      echo '<h1>Someone has stolen your credentials.  All saved logins for your account have been erased.  Please login again.</h1>';
      fail($user . '\'s credentials have been stolen.  All tokens have been deleted.');
    }
    // generate new token
    $token = openssl_random_pseudo_bytes(20);
    update_token($user, $serial, $token);
    $cookie = $user . ':' . $serial . ':' . hash_hmac('sha256', $token, $remembermekey);
    setcookie('rememberme', $cookie, strtotime('+' . $cookie_age . ' days'));
//    $_SESSION['id'] = $user;
    set_current_id($user);	// shellsink allows multiple IDs under one user
    return true;
  }
  return false;
}

function remember_me() {
  global $remembermekey, $cookie_age;
//  $user = $_SESSION['id'];
  $user = $_SESSION['mainid'];	// shellsink allows multiple IDs under one user
  $serial = openssl_random_pseudo_bytes(20);
  $token = openssl_random_pseudo_bytes(20);
  store_token(pack('H*', $user), $serial, $token);
  $cookie = $user . ':' . bin2hex($serial) . ':' . hash_hmac('sha256', $token, $remembermekey);
  setcookie('rememberme', $cookie, strtotime('+' . $cookie_age . ' days'));
}

function store_token($user_id, $serial, $token) {
  global $db, $cookie_age;

  $params = array(
    ':user_id' => $user_id,
    ':serial' => $serial,
    ':token' => $token,
  );

  try {
    connect_sql();

    $stmt = $db->prepare('insert into tokens (user_id, serial, token, expires) values (:user_id, :serial, :token, now() + interval ' . $cookie_age . ' day)');
    $stmt->execute($params);
    $db = null;

  } catch(PDOException $e) {
    fail($e->getMessage());
  }
}

function get_token($user_id, $serial) {
  global $db;

  $params = array(
    ':user_id' => pack('H*', $user_id),
    ':serial' => pack('H*', $serial),
  );

  try {
    connect_sql();
    $stmt = $db->prepare('select token from tokens where user_id = :user_id and serial = :serial and expires > now()');
    $stmt->execute($params);
    $row = $stmt->fetch();
    $db = null;

  } catch(PDOException $e) {
    fail($e->getMessage());
  }

  return $row['token'];
}

function update_token($user_id, $serial, $token) {
  global $db, $cookie_age;

  $params = array(
    ':user_id' => pack('H*', $user_id),
    ':serial' => pack('H*', $serial),
    ':token' => $token,
  );

  try {
    connect_sql();

    $stmt = $db->prepare('update tokens set token = :token, expires = now() + interval ' . $cookie_age . ' day where user_id = :user_id and serial = :serial');
    $stmt->execute($params);
    $db = null;

  } catch(PDOException $e) {
    fail($e->getMessage());
  }
}

function delete_tokens($user_id) {
  global $db;

  $params = array(
    ':user_id' => pack('H*', $user_id),
  );

  try {
    connect_sql();

    $stmt = $db->prepare('delete from tokens where user_id = :user_id');
    $stmt->execute();
    $db = null;

  } catch(PDOException $e) {
    fail($e->getMessage());
  }
}

function delete_token($user_id, $serial) {
  global $db;

  $params = array(
    ':user_id' => pack('H*', $user_id),
    ':serial' => pack('H*', $serial),
  );

  try {
    connect_sql();

    $stmt = $db->prepare('delete from tokens where user_id = :user_id and serial = :serial');
    $stmt->execute();
    $db = null;

  } catch(PDOException $e) {
    fail($e->getMessage());
  }
}

function cleanup_tokens() {
  global $db;

  try {
    connect_sql();

    $stmt = $db->prepare('delete from tokens where expires < now()');
    $stmt->execute();
    $db = null;

  } catch(PDOException $e) {
    fail($e->getMessage());
  }
}

function count_tokens($user_id) {
  global $db;

  $params = array(
    ':user_id' => pack('H*', $user_id),
  );

  try {
    connect_sql();

    $stmt = $db->prepare('select count(token) from tokens where user_id = :user_id');
    $stmt->execute($params);
    $count = $stmt->fetchColumn();
    $db = null;

  } catch(PDOException $e) {
    fail($e->getMessage());
  }

  return $count;
}
