<?php

require '../config.php';
require '../shellsink.php';

$command = strtok($_SERVER['REQUEST_URI'], '?');

if (substr($command, -1) == '/') {
  $command = '/';
} else {
  if (dirname($command) != '/') {
    // strip subfolder
    $command = '/' . basename($command);
  }
}

route($command);

