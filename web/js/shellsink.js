var max_filters = 5;
var filterSelected = [];

$ = function(id) {
  return document.getElementById(id);
}

function httpSend(url, data, async) {
  async = typeof async !== "undefined" ? async : false;

  if (window.XMLHttpRequest) {
    // code for IE7+, Firefox, Chrome, Opera, Safari
    var xmlhttp = new XMLHttpRequest();
  } else {
    // code for IE6, IE5
    var xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
  }

  // GET
//  var curTime = new Date().getTime();
//  xmlhttp.open("GET", url + "?" + data + "&_=" + curTime, async);
//  xmlhttp.send();

  // POST
  xmlhttp.open("POST", url, async);
  xmlhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
  xmlhttp.send(data);

  return xmlhttp.responseText;
}

function disableAtom() {
  httpSend('setAtomPreference', 'disable_atom=' + $('disable_atom').checked, true);
}

function disableAtomSelect(element) {
  if (element.checked == true) {
    $('filter').disabled = true;
  } else {
    $('filter').disabled = false;
  }
  disableAtom();
}

function updateFilter(senddata) {
  if (typeof senddata === 'undefined') { senddata = true; }
  var parms = '';
  var filter;
  if ($('filter')[0].selected == true) {
    filterSelected = [];
  } else {
    for (i=1; i<$('filter').length; i++) {
      loc = filterSelected.indexOf($('filter')[i].value);
      if ($('filter')[i].selected) {
        filter = $('filter')[i].text;
        if (loc == -1) {
          if (filterSelected.length >= max_filters) {
            $('filter')[i].selected = false;
            alert('Please limit the filters to ' + max_filters);
            return;
          }
          filterSelected.push($('filter')[i].value);
        }
        parms += 'filter[]=' + $('filter')[i].value + '&';
      } else if (loc != -1) {
        filterSelected.splice(loc, 1);
      }
    }
  }
  if (filterSelected.length > 0) {
    if (filterSelected.length == 1) {
      $('atomurl').innerHTML = '&amp;filter=' + filter;
    } else {
      $('atomurl').innerHTML = '&amp;filters=[' + filterSelected.join() + ']';
      if ($('and').checked == true) {
        $('atomurl').innerHTML += '&amp;and=1';
      }
    }
    $('buildfilter').href = $('atombase').innerHTML + $('atomurl').childNodes[0].nodeValue;
  } else {
    parms = 'filter[]=""';
    $('atomurl').innerHTML = '';
    $('buildfilter').href = $('atombase').innerHTML;
  }
  if (senddata) {
    httpSend('setAtomPreference', parms.slice(0, -1), true);
  }
}

function andSearch(element) {
  if (element.checked == true) {
    if (filterSelected.length > 1 && $('atomurl').innerHTML.slice(-10) != '&amp;and=1') {
      $('atomurl').innerHTML += '&and=1';
    }
  } else if ($('atomurl').innerHTML.slice(-10) == '&amp;and=1') {
    $('atomurl').innerHTML = $('atomurl').innerHTML.slice(0,-10);
  }
  if ($('atomurl').innerHTML != '') {
    $('buildfilter').href = $('atombase').innerHTML + $('atomurl').childNodes[0].nodeValue;
  } else {
    $('buildfilter').href = $('atombase').innerHTML;
  }
}

function editAnnotation(index) {
  $('annotation'+index).style.display = 'none';
  $('annotation-input-div'+index).style.display = 'block';
}

function updateAnnotation(index) {
  httpSend('addAnnotation', 'annotation=' + $('annotation-input'+index).value + '&id=' + $('id'+index).value, true);
  $('annotation'+index).innerHTML = $('annotation-input'+index).value;
  $('annotation'+index).style.display = 'block';
  $('annotation-input-div'+index).style.display = 'none';
}

function toggleManageIDs() {
  if ($('ManageIDs').style.display == 'none') {
    $('ManageIDs').style.display = '';
  } else {
    $('ManageIDs').style.display = 'none';
  }
}
