<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <title>Shell-sink PHP</title>
  <link href="https://fonts.googleapis.com/css?family=Roboto" rel="stylesheet" type="text/css">
  <link href="css/login.css" rel="stylesheet">
  <link href="css/style.css" rel="stylesheet">
  <link href="img/favicon.ico" rel="icon">
</head>
<body>
<?php if (!$authenticated) { ?>
  <div class="right">Login with: <a href="auth">
  <div id="customBtn" class="customGPlusSignIn">
    <span class="icon"></span>
    <span class="buttonText">Google</span>
  </div></a>
  </div>
<?php } else { ?>
  <div class="right"><a href="logout">
  <div id="customBtn" class="customGPlusSignIn gray">
    <span class="icon gray"></span>
    <span class="buttonText">Logout</span>
  </div></a>
  </div>
<a href="history">History</a> | <a href="preferences">Preferences</a><br>
<?php } ?>
<h1>Shellsink</h1>
<div>
Shellsink is a web accessible copy of your bash history.  Get the code <a 
href="https://bitbucket.org/jzielke/shellsink-php">here</a>.<br>
<br>
How to use it:<br>
Login with Google to create an account.  You will be taken to the preferences page where you will see your 
secret ID string.  Install the <a href="shellsink-client">shellsink-client</a> in your path and add the 
displayed code to your .bashrc.  After you run some commands you should find a searchable index of all of 
your commands on the <a href="history">history</a> page.  Commands are automatically tagged by folder, 
hostname and tty.
