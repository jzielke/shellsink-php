<div class='preferences'>
  <ul>
    <li>Your unique ID is: <span class='hash'><?=$_SESSION['id']?></span> (<?=$_SESSION['name']?>) | <span class="u" onclick="toggleManageIDs();">Manage IDs</span><br>
    <div id="ManageIDs" style="display: none;">
    Maximum <?=$max_user_ids?> IDs<br>
    <?php foreach ($ids as $id) { ?>
    <a href="<?=$path?>/changeid?user_id=<?=$id[0]?>"><?=$id[0]?> (<?=$id[1]?>)</a><br>
    <?php } ?>
    <?php if (count($ids) < $max_user_ids) { ?>
    <form method="post" action="newid"><br>
    <input name="name" type="text" size="10" placeholder="Alias"> <input type="submit" value="New ID">
    </form>
    <?php } ?>
    <br></div></li>
    <li>Download <a href="shellsink-client">shellsink-client</a></li>
    <li><label>Disable atom feed: <input id="disable_atom" type="checkbox" name="disable_atom" <?php if ($disable_atom) echo 'checked'; ?> onchange="disableAtomSelect(this)"></label></li>
    <li>Atom feed URL builder (limit <?=$max_filters?>, first option = no filter):<br>
        <label>AND search (leave unchecked for OR search) <input id="and" type="checkbox" name="and" onchange="andSearch(this)"></label><br>
        <a id="buildfilter" href='<?=$filterurl1?><?=$filterurl2?>'><span id="atombase">https://<?=$_SERVER['HTTP_HOST']?><?=$filterurl1?></span><span id="atomurl"><?=$filterurl2?></span></a><br>
    <select multiple size="10" id="filter" style="min-width: 500px;" onchange="updateFilter()" <?php if ($disable_atom) echo 'disabled="true"'; ?>>
      <option value=""<?php if ($filter[0] == '') echo ' selected';?>>&nbsp;</option>
      <?php foreach ($tags as $tag) { ?>
      <option value="<?=$tag[0]?>"<?php if (in_array($tag[0], $filter)) echo ' selected="yes"';?>><?php echo htmlspecialchars($tag[1]); ?></option>
      <?php } ?>
    </select></li>
  </ul>
</div>
<div>
<span class="b">.bashrc</span><br>
<pre class="bash">
shopt -s histappend
tty=$(tty)
export SHELL_SINK_COMMAND=shellsink-client
export SHELL_SINK_ID=<?=$_SESSION['id']?>

SHELL_SINK_TAGS=$(hostname -s):${tty:5}
if [ -n "$SSH_CONNECTION" ]; then
  sshcon=${SSH_CONNECTION%% *}
  sshcon=${sshcon//:/-}
  SHELL_SINK_TAGS=$SHELL_SINK_TAGS:ssh:$sshcon
elif [ -n "$SSH_CLIENT" ]; then
  sshcon=${SSH_CLIENT%% *}
  sshcon=${sshcon//:/-}
  SHELL_SINK_TAGS=$SHELL_SINK_TAGS:ssh:$sshcon
fi
export SHELL_SINK_TAGS
export SHELL_SINK_URL='https://<?=$_SERVER['HTTP_HOST']?><?=$path?>'
prompt_command() { EXITSTATUS=$?; history -a; $SHELL_SINK_COMMAND -s $EXITSTATUS; }
PROMPT_COMMAND='prompt_command'
alias nosink='unset PROMPT_COMMAND'
touch ~/.shellsink_new_login
</pre>
<br>
<span class="b">.zshrc</span><br>
These need to be set to something; HISTFILE needs to be exported:<br>
<pre class="bash">
export HISTFILE=~/.histfile
HISTSIZE=1000
SAVEHIST=1000
</pre><br>
Copy and paste the rest:<br>
<pre class="bash">
setopt inc_append_history
setopt interactivecomments
tty=$(tty)
export SHELL_SINK_COMMAND=shellsink-client
export SHELL_SINK_ID=<?=$_SESSION['id']?>

SHELL_SINK_TAGS=$(hostname -s):${tty[6,-1]}
if [[ -n "$SSH_CONNECTION" ]]; then
  sshcon=${SSH_CONNECTION%% *}
  sshcon=${sshcon//:/-}
  SHELL_SINK_TAGS=${SHELL_SINK_TAGS}:ssh:$sshcon
elif [[ -n "$SSH_CLIENT" ]]; then
  sshcon=${SSH_CLIENT%% *}
  sshcon=${sshcon//:/-}
  SHELL_SINK_TAGS=${SHELL_SINK_TAGS}:ssh:$sshcon
fi
export SHELL_SINK_TAGS
export SHELL_SINK_URL='https://<?=$_SERVER['HTTP_HOST']?><?=$path?>'
function precmd { $SHELL_SINK_COMMAND -s $? }
alias nosink='function precmd {}'
touch ~/.shellsink_new_login
</pre>
</div>
<div>
<script>
  updateFilter(false);
</script>
</div>
