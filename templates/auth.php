<?php
require_once 'Google/autoload.php';

$client = new Google_Client();
$client->setApplicationName("Shell-sink");
$client->setClientId($GoogleId);
$client->setClientSecret($GoogleSecret);
$client->setRedirectUri('https://' . $_SERVER['HTTP_HOST'] . $path . '/auth');
$client->setApprovalPrompt('auto');
$client->setAccessType('offline');
$client->setScopes(array('email', 'profile'));
$oauth2 = new Google_Service_Oauth2($client);
if (isset($_GET['code'])) {
  $client->authenticate($_GET['code']);
  $_SESSION['token'] = $client->getAccessToken();
  $redirect_uri = '//' . $_SERVER['HTTP_HOST'] . $path . '/';
  header('Location: ' . filter_var($redirect_uri, FILTER_SANITIZE_URL));
}
if (isset($_SESSION['token'])) {
  $client->setAccessToken($_SESSION['token']);
}
if (isset($_REQUEST['error'])) {
  echo '<script type="text/javascript">window.close();</script>';
  exit;
}
if ($client->getAccessToken()) {
  $user = $oauth2->userinfo->get();
  $email = filter_var($user['email'], FILTER_SANITIZE_EMAIL);
  $name = $user['name'];
  $link = 'https://plus.google.com/' . $user['id'];
  $redirect = verify_or_create($email, $name, $link);
  if ($set_login_cookie) remember_me();
  $_SESSION['token'] = $client->getAccessToken();
  $redirect_uri = '//' . $_SERVER['HTTP_HOST'] . $path . $redirect;
  header('Location: ' . filter_var($redirect_uri, FILTER_SANITIZE_URL));
} else {
  $auth_url = $client->createAuthUrl();
  header('Location: ' . filter_var($auth_url, FILTER_SANITIZE_URL));
}
