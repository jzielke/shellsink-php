<?php
require_once 'Google/autoload.php';

$cookie = isset($_COOKIE['rememberme']) ? $_COOKIE['rememberme'] : '';
if (isset($_REQUEST['all']) && isset($_SESSION['id'])) {
  delete_tokens($_SESSION['mainid']);
} elseif ($cookie) {
  list ($user, $serial, $mac) = explode(':', $cookie);
  if (isset($_REQUEST['all'])) {
    delete_tokens($user);
  } else {
    delete_token($user, $serial);
  }
}
if ($cookie) {
  setcookie('rememberme', $cookie, 1);
}

$client = new Google_Client();
unset($_SESSION['token']);
unset($_SESSION['id']);
$client->revokeToken();

$redirect_uri = '//' . $_SERVER['HTTP_HOST'] . $path . '/';
header('Location: ' . filter_var($redirect_uri, FILTER_SANITIZE_URL));
