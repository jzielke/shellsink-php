<!DOCTYPE html>
<!--
This file is part of Shell-Sink.
Copyright Joshua Cronemeyer 2008, 2009

Shell-Sink is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Shell-Sink is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License v3 for more details.

You should have received a copy of the GNU General Public License
along with Shell-Sink.  If not, see <http://www.gnu.org/licenses/>.
-->
<html>
<head>
  <meta charset="utf-8">
  <title>Shell-sink PHP</title>
  <link href="css/style.css" rel="stylesheet">
  <link href="img/favicon.ico" rel="icon">
  <script src="js/shellsink.js"></script>
</head>
<body>
  <div class="bottom-border" id="header">
    <form action="commandSearch" method="post">
      Welcome, <?=$name?> |
      <a href='<?=$path?>/history'>History</a> |
      <a href='<?=$path?>/preferences'>Preferences</a> |
      <a href='<?=$path?>/logout'>Logout</a>
      <?php if ($token_count > 1) { ?> | <a href='<?=$path?>/logout?all'>Logout All</a> <?php } ?>
      <span class="align-right">
        <label for="query">Search Commands</label>
        <input type="text" name="query" id="query" size="25" value="<?php if (isset($query)) echo $query; ?>" onkeyup="limitSearchTerms(this)">
        <input type="submit" id="querySubmit" Value='search'>
      </span>
    </form>
  </div>
  <div id="content">
