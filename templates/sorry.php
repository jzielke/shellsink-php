<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <title>Shell-sink PHP</title>
  <link href="https://fonts.googleapis.com/css?family=Roboto" rel="stylesheet" type="text/css">
  <link href="css/login.css" rel="stylesheet">
  <link href="css/style.css" rel="stylesheet">
  <link href="img/favicon.ico" rel="icon">
</head>
<body>
<h1>New User Registration Disabled</h1>
<div>
I am sorry but this shellsink server does not accept new registrations.<br><br>
If this is your server and you want to enable new user registration edit config.php and set $enable_registration to 1.<br><br>
If you would like the code to run your own shellsink server you can get it <a 
href="https://bitbucket.org/jzielke/shellsink-php">here</a>.
