<?=$pagination?>

<!-- block heading -->
<div class="command-heading">
  <span class='red-heading'><?=$heading1?></span> <?=$heading2?>
</div>

<!-- block content -->
<?php if (count($commands) == 0) { ?>
  <div class="red-warning">
    shell_sink hasn't recieved any commands from you yet.  Make sure your client is configured correctly.
    See <a href='https://<?=$_SERVER['HTTP_HOST']?><?=$path?>/preferences'>here</a> for instructions.
  </div>
<?php
} else {
  foreach ($commands as $key => $command) {
?>
    <div class='command'>
      <span class="hash b"><?php if ($command['status']) echo $command['status'] . '&nbsp;'; ?></span>
      <span class='command-text'><?php echo htmlspecialchars($command['command']); ?></span>&nbsp;-
      <span class='time' title="<?php echo date('l, F j, Y \a\t g:ia T', strtotime($command['date'])) ?>"><?php echo time_since(strtotime($command['date'])) ?> ago.</span>

      <div id='annotation-block<?=$key?>'>
        <div class='command-annotation' id='annotation<?=$key?>'<?php if ($command['annotation'] == '') echo ' style="display: none;"'; ?>><?php echo htmlspecialchars($command['annotation']); ?></div>
        <div style="display: none;" id='annotation-input-div<?=$key?>'>
          <textarea id='annotation-input<?=$key?>' rows='10' cols='80'><?php if ($command['annotation'] != '') echo $command['annotation']; ?></textarea>
          <img src="img/button.png" alt="X" onclick="updateAnnotation(<?=$key?>)"/>
        </div>
      </div>
      <img class='annotate-img' src="img/annotate.png" alt="annotate" onclick="editAnnotation(<?=$key?>)"/>
      <span class='tag-heading'>Tags:</span> <img src='img/tag.png' alt="tag" onclick="showQuickTag(<?=$key?>)"/>
      <span id='tags<?=$key?>'><?php foreach ($command['tags'] as $tag) { ?><a href='<?=$path?>/showTag?tag=<?php echo urlencode($tag); ?>'><?=$tag?></a> <?php } ?></span>
      <span id='quick-tag<?=$key?>' style="display: none;">
        <input type="text" id="tagvalue<?=$key?>" size="25">
        <input type="hidden" value="<?=$command['id']?>" id="id<?=$key?>">
        <img src="img/button.png" alt="X" onclick="updateTag(<?=$key?>)" />
      </span>
    </div>
<?php
  }
}
echo $pagination;
?>
