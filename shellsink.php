<?php

function gen_uuid() {
  return pack('H*', sprintf( '%04x%04x%04x%04x%04x%04x%04x%04x',
    mt_rand(0, 0xffff), mt_rand(0, 0xffff), mt_rand(0, 0xffff),
    mt_rand(0, 0x0fff) | 0x4000,
    mt_rand(0, 0x3fff) | 0x8000,
    mt_rand(0, 0xffff), mt_rand(0, 0xffff), mt_rand(0, 0xffff)
 ));
}

function time_since($original) {
  // http://web.archive.org/web/20060617175230/http://blog.natbat.co.uk/archive/2003/Jun/14/time_since
  // array of time period chunks
  $chunks = array(
    array(31536000, 'year'),	// 60 * 60 * 24 * 365
    array(2592000, 'month'),	// 60 * 60 * 24 * 30
    array(604800, 'week'),	// 60 * 60 * 24 * 7
    array(86400, 'day'),	// 60 * 60 * 24
    array(3600, 'hour'),	// 60 * 60
    array(60, 'minute'),
  );

  $today = time(); /* Current unix time in seconds  */
  $since = $today - $original;

  // $j saves performing the count function each time around the loop
  for ($i = 0, $j = count($chunks); $i < $j; $i++) {

    $seconds = $chunks[$i][0];
    $name = $chunks[$i][1];

    // finding the biggest chunk (if the chunk fits, break)
    if (($count = floor($since / $seconds)) != 0) {
        break;
    }
  }

  $print = ($count == 1) ? '1 '.$name : "$count {$name}s";

  if ($i + 1 < $j) {
    // now getting the second item
    $seconds2 = $chunks[$i + 1][0];
    $name2 = $chunks[$i + 1][1];

    // add second item if it's count greater than 0
    if (($count2 = floor(($since - ($seconds * $count)) / $seconds2)) != 0) {
      $print .= ($count2 == 1) ? ', 1 '.$name2 : ", $count2 {$name2}s";
    }
  }

  return $print;
}

function xmlspecialchars($xml) {
  // http://www.w3.org/TR/REC-xml/#sec-predefined-ent
  $xmltrans = array(
    '<' => '&lt;',
    '>' => '&gt;',
    '&' => '&amp;',
    "'" => '&apos;',
    '"' => '&quot;',
  );
  return strtr($xml, $xmltrans);
}

function is_authenticated() {
  global $debug, $set_login_cookie;

  // already authenticated?
  if (isset($_SESSION['id']) && $_SESSION['id']) return true;

  // check for rememberme cookie
  if ($set_login_cookie == 1) {
    require '../rememberme.php';
    return check_rememberme_cookie();
  }

  return false;
}

function check_user($user_id) {
  global $db, $debug;

  $params = array(
    ':user_id' => pack('H*', $user_id),
    ':alt_id' => pack('H*', $user_id),
  );

  try {
    connect_sql();

    $stmt = $db->prepare('select count(*) from users left join identities on users.user_id = identities.user_id where users.user_id = :user_id or alt_id = :alt_id');
    $stmt->execute($params);
    $count = $stmt->fetchColumn();
    $db = null;

    if ($count < 1) {
      if ($debug) debug_msg('Unknown user: ' . $user_id);
      return false;
    }

  } catch(PDOException $e) {
    fail($e->getMessage());
  }

  return true;
}

function route($command) {
  global $debug, $path, $tracking, $cleanupkey, $set_login_cookie;

  session_start();

  // keep search query
  if (isset($_REQUEST['query'])) {
    $query = $_REQUEST['query'];
    $_SESSION['query'] = $query;
  } elseif (isset($_SESSION['query'])) {
    $query = $_SESSION['query'];
  }

  // get page number
  if (isset($_REQUEST['page'])) {
    $page = $_REQUEST['page'];
  } else {
    $page = 1;
  }

  switch ($command) {
    case '/addCommand':
      if (!check_user($_REQUEST['hash'])) {
        header('HTTP/1.0 404 Not Found');
        die("The account requested doesn't exist.");
      }
      if (isset($_REQUEST['status'])) {
        $status = $_REQUEST['status'];
      } else {
        $status = null;
      }
      $command_id = add_command($_REQUEST['hash'], $_REQUEST['command'], $status);
      foreach($_REQUEST['tags'] as $tag) {
        $tag_id = get_or_set_tag($tag);
        link_tag($tag_id, $command_id);
      }
      break;
    case '/history':
      if (!is_authenticated()) {
        $redirect_uri = '//' . $_SERVER['HTTP_HOST'] . $path . '/';
        header('Location: ' . filter_var($redirect_uri, FILTER_SANITIZE_URL));
        exit;
      }
      $heading1 = 'Commands Issued:';
      $heading2 = '';
      list ($name, $email, $disable_atom, $filter) = get_user($_SESSION['id']);
      $commands = fetch_commands($_SESSION['id'], $page);
      $pages = fetch_commands_pages($_SESSION['id']);
      include '../lopa.php';
      $url = paginationURL($_SERVER['QUERY_STRING']);
      if ($pages > 1) {
        $pagination = paginationHTML($page, $pages, $url);
      } else {
        $pagination = '';
      }
      if ($set_login_cookie == 1) {
        require_once '../rememberme.php';
        $token_count = count_tokens($_SESSION['mainid']);
      } else {
        $token_count = 0;
      }
      include '../templates/header.php';
      include '../templates/history.php';
      include '../templates/footer.php';
      break;
    case '/showTag':
      if (!is_authenticated()) {
        $redirect_uri = '//' . $_SERVER['HTTP_HOST'] . $path . '/';
        header('Location: ' . filter_var($redirect_uri, FILTER_SANITIZE_URL));
        exit;
      }
      if (isset($_REQUEST['tag'])) {
        $heading1 = 'Commands tagged with:';
        $heading2 = $_REQUEST['tag'];
        list ($name, $email, $disable_atom, $filter) = get_user($_SESSION['id']);
        $commands = fetch_commands_by_filter($_SESSION['id'], $_REQUEST['tag'], $page);
        $pages = fetch_commands_by_filter_pages($_SESSION['id'], $_REQUEST['tag']);
        include '../lopa.php';
        $url = paginationURL($_SERVER['QUERY_STRING']);
        if ($pages > 1) {
          $pagination = paginationHTML($page, $pages, $url);
        } else {
          $pagination = '';
        }
        if ($set_login_cookie == 1) {
          require_once '../rememberme.php';
          $token_count = count_tokens($_SESSION['mainid']);
        } else {
          $token_count = 0;
        }
        include '../templates/header.php';
        include '../templates/history.php';
        include '../templates/footer.php';
      }
      break;
    case '/commandSearch':
      if (!is_authenticated()) {
        $redirect_uri = '//' . $_SERVER['HTTP_HOST'] . $path . '/';
        header('Location: ' . filter_var($redirect_uri, FILTER_SANITIZE_URL));
        exit;
      }
      if (isset($query)) {
        $heading1 = 'Commands matching:';
        $heading2 = $query;
        list ($name, $email, $disable_atom, $filter) = get_user($_SESSION['id']);
        $commands = fetch_commands_by_search($_SESSION['id'], $query, $page);
        $pages = fetch_commands_by_search_pages($_SESSION['id'], $query);
        include '../lopa.php';
        $url = paginationURL($_SERVER['QUERY_STRING']);
        if ($pages > 1) {
          $pagination = paginationHTML($page, $pages, $url);
        } else {
          $pagination = '';
        }
        if ($set_login_cookie == 1) {
          require_once '../rememberme.php';
          $token_count = count_tokens($_SESSION['mainid']);
        } else {
          $token_count = 0;
        }
        include '../templates/header.php';
        include '../templates/history.php';
        include '../templates/footer.php';
      }
      break;
    case '/preferences':
      global $max_filters, $max_user_ids;
      if (is_authenticated()) {
        list ($name, $email, $disable_atom, $filter) = get_user($_SESSION['id']);
      } else {
        $redirect_uri = '//' . $_SERVER['HTTP_HOST'] . $path . '/';
        header('Location: ' . filter_var($redirect_uri, FILTER_SANITIZE_URL));
        exit;
      }
      $tags = get_tags($_SESSION['id']);
      if ($set_login_cookie == 1) {
        require_once '../rememberme.php';
        $token_count = count_tokens($_SESSION['mainid']);
      } else {
        $token_count = 0;
      }
      $filterurl1 = $path . '/atom?user_id=' . $_SESSION['id'];
      if (count($filter) == 1 && !empty($filter[0])) {
        $filterurl2 = '&amp;filter=' . get_tag($filter[0]);
      } elseif (count($filter) > 1) {
        $filterurl2 = '&amp;filters=' . json_encode($filter, true);
      } else {
        $filterurl2 = '';
      }
      $ids = get_ids();
      include '../templates/header.php';
      include '../templates/preferences.php';
      include '../templates/footer.php';
      break;
    case '/':
      $authenticated = is_authenticated();
      include '../templates/index.php';
      include '../templates/footer.php';
      break;
    case '/auth':
      global $set_login_cookie, $GoogleId, $GoogleSecret;
      if ($set_login_cookie == 1) require '../rememberme.php';
      include '../templates/auth.php';
      break;
    case '/addAnnotation':
      if (!is_authenticated()) {
        header('HTTP/1.0 404 Not Found');
        die("Not logged in.");
      }
      if (isset($_REQUEST['annotation']) && isset($_REQUEST['id'])) {
        add_annotation($_SESSION['id'], $_REQUEST['id'], $_REQUEST['annotation']);
      }
      break;
    case '/shellsink-client':
      $client = '../client/shellsink-client';
      $last_modified_time = filemtime($client);
      $etag = md5_file($client);
      header('Content-Type: text/plain');
      header('Content-Length: ' . filesize($client));
      header('Last-Modified: '.gmdate('D, d M Y H:i:s', $last_modified_time).' GMT');
      header('Etag: ' . $etag);
      if (@strtotime($_SERVER['HTTP_IF_MODIFIED_SINCE']) == $last_modified_time ||
        @trim($_SERVER['HTTP_IF_NONE_MATCH']) == $etag) {
        header('HTTP/1.1 304 Not Modified');
        exit;
      }
      $fp = fopen($client, 'rb');
      fpassthru($fp);
      break;
    case '/pull':
      if (!check_user($_REQUEST['hash'])) {
        header('HTTP/1.0 404 Not Found');
        die();
      }
      if (isset($_REQUEST['tag'])) {
        $commands = pull_by_tag($_REQUEST['hash'], $_REQUEST['tag']);
      } elseif (isset($_REQUEST['keyword'])) {
        $commands = pull_by_keyword($_REQUEST['hash'], $_REQUEST['keyword']);
      } else {
        $commands = pull_by_id($_REQUEST['hash']);
      }
      header('Content-Type: text/xml');
      echo '<shellsink-commands>' . PHP_EOL;
      foreach ($commands as $command)
        echo $command;
      echo '</shellsink-commands>' . PHP_EOL;
      break;
    case '/atom':
      global $max_filters;
      if (!isset($_REQUEST['user_id']) || !check_user($_REQUEST['user_id'])) {
        header('HTTP/1.0 404 Not Found');
        die();
      }
      header('Content-Type: text/xml');
      $user_id = $_REQUEST['user_id'];
      list ($name, $email, $disable_atom, $filter) = get_user($user_id);
      $update_time = date('Y-m-d H:i:s');
      $year = date('Y');
      if ($disable_atom) {
        $commands = array();
      } else {
        if (isset($_REQUEST['and']) && $_REQUEST['and'] == 1) {
          $and = True;
        } else {
          $and = False;
        }
        if (empty($_REQUEST['filter']) && empty($_REQUEST['filters'])) {
          $commands = fetch_commands($user_id, 1, True);
        } elseif (!empty($_REQUEST['filter'])) {
          $commands = fetch_commands_by_filter($user_id, $_REQUEST['filter'], 1, True, $and);
        } else {
          $commands = fetch_commands_by_filter($user_id, array_slice(json_decode($_REQUEST['filters']), 0, $max_filters), 1, True, $and);
        }
      }
      if (count($commands) > 0) {
        $update_time = $commands[0]['date'];
      }
      include '../templates/atom.xml';
      break;
    case '/setAtomPreference':
      if (!is_authenticated()) {
        header('HTTP/1.0 404 Not Found');
        die("Not logged in.");
      }
      set_atom_preference($_SESSION['id']);
      break;
    case '/showCommand':
      if (!is_authenticated()) {
        $redirect_uri = '//' . $_SERVER['HTTP_HOST'] . $path . '/';
        header('Location: ' . filter_var($redirect_uri, FILTER_SANITIZE_URL));
        exit;
      }
      if (isset($_REQUEST['id'])) {
        $heading1 = 'Command:';
        $heading2 = '';
        list ($name, $email, $disable_atom, $filter) = get_user($_SESSION['id']);
        $commands = fetch_command_by_id($_SESSION['id'], $_REQUEST['id']);
        $pagination = '';
        if ($set_login_cookie == 1) {
          require_once '../rememberme.php';
          $token_count = count_tokens($_SESSION['mainid']);
        } else {
          $token_count = 0;
        }
        include '../templates/header.php';
        include '../templates/history.php';
        include '../templates/footer.php';
      }
      break;
    case '/logout':
      if ($set_login_cookie == 1) require '../rememberme.php';
      include '../templates/logout.php';
      break;
    case '/changeid':
      if (!is_authenticated()) {
        header('HTTP/1.0 404 Not Found');
        die("Not logged in.");
      }
      change_id();
      break;
    case '/newid':
      if (!is_authenticated()) {
        header('HTTP/1.0 404 Not Found');
        die("Not logged in.");
      }
      new_id();
      break;
    case '/sorry':
      include '../templates/sorry.php';
      include '../templates/footer.php';
      break;
    case '/cleanup':
      if (!isset($_REQUEST['auth']) || $_REQUEST['auth'] !== $cleanupkey) {
        if ($debug) debug_msg('cleanup tokens request failed authentication - exiting instead');
        header('HTTP/1.0 404 Not Found');
        die();
      }
      if ($debug) debug_msg('cleaning up expired tokens');
      require '../rememberme.php';
      cleanup_tokens();
      break;
    default:
      fail('I do not know how to ' . $command);
  }
}

function add_command($user_id, $command, $status) {
  global $db, $debug;

  $params = array(
    ':command_id' => gen_uuid(),
    ':user_id' => pack('H*', $user_id),
    ':command' => $command,
    ':status' =>  $status,
  );

  try {
    connect_sql();
    $stmt = $db->prepare('insert into commands (command_id, user_id, command, status) values (:command_id, :user_id, :command, :status)');
    $stmt->execute($params);
    $db = null;

  } catch(PDOException $e) {
    fail($e->getMessage());
  }

  return $params[':command_id'];
}

function verify_or_create($email, $name, $link) {
  global $db, $debug, $enable_registration, $path;

  try {
    connect_sql();

    // verify
    $params = array(
      ':url' => $link,
    );
    $stmt = $db->prepare('select user_id, name, email from users where url = :url');
    $stmt->execute($params);
    $count = $stmt->rowCount();

    if ($count == 0) {
      if (!$enable_registration) {
        $redirect_uri = '//' . $_SERVER['HTTP_HOST'] . $path . '/sorry';
        header('Location: ' . filter_var($redirect_uri, FILTER_SANITIZE_URL));
        if ($debug) debug_msg('Declining new user registration for ' . $name . ', ' . $link);
        exit;
      }
      // create
      $UUID = md5($name . microtime(true) . mt_rand(0, 0xffff));
      $params = array(
        ':user_id' =>  pack('H*', $UUID),
        ':email' => $email,
        ':name' => $name,
        ':url' => $link,
      );
      $stmt = $db->prepare('insert into users (user_id, created, name, email, url) values (:user_id, NOW(), :name, :email, :url)');
      $stmt->execute($params);
      $db = null;
      $_SESSION['id'] = $UUID;
      $_SESSION['mainid'] = $UUID;
      $_SESSION['name'] = 'Primary';
      if ($debug) debug_msg('Created user: ' . $name . ', ' . $link);
      return '/preferences';
    } else {
      // returning user
      $row = $stmt->fetch();
      if ($email !== $row['email'] || $name !== $row['name']) {
        // update profile
        if ($debug) debug_msg('updating profile for ' . $email);
        $params = array(
          ':email' => $email,
          ':name' => $name,
          ':url' => $link,
        );
        $stmt = $db->prepare('update users set name = :name, email = :email where url = :url');
        $stmt->execute($params);
        $db = null;
      }
      set_current_id(bin2hex($row['user_id']));
      return '/history';
   }

  } catch(PDOException $e) {
    fail($e->getMessage());
  }
}

function set_current_id($user_id) {
  global $db, $debug;

  $_SESSION['mainid'] = $user_id;

  try {
    connect_sql();

    $params = array(
      ':user_id' => pack('H*', $user_id),
    );
    $stmt = $db->prepare('select alt_id, name from identities where active = 1 and user_id = :user_id');
    $stmt->execute($params);
    $count = $stmt->rowCount();
    if ($count > 0) {
      $row = $stmt->fetch();
      $db = null;
      $_SESSION['id'] = bin2hex($row['alt_id']);
      $_SESSION['name'] = $row['name'];
      if ($debug) debug_msg('using alternate id ' . $_SESSION['id']);
    } else {
      $_SESSION['id'] = $user_id;
      $_SESSION['name'] = 'Primary';
      if ($debug) debug_msg('using primary id ' . $_SESSION['id']);
    }
  } catch(PDOException $e) {
    fail($e->getMessage());
  }
}

function get_user($user_id) {
  global $db, $debug;

  $params = array(
    ':user_id' => pack('H*', $user_id),
  );

  try {
    connect_sql();

    if ($_SESSION['id'] != $_SESSION['mainid']) {
      $stmt = $db->prepare('select user_id, filter from identities where alt_id = :user_id');
      $stmt->execute($params);
      $row = $stmt->fetch();
      $params = array(
        ':user_id' => $row['user_id'],
      );
      $stmt = $db->prepare('select name, email, atom from users where user_id = :user_id');
      $stmt->execute($params);
      $row2 = $stmt->fetch();
      $db = null;
      if (empty($row['filter'])) $row['filter'] = '[""]';
      return array($row2['name'], $row2['email'], !$row2['atom'], json_decode($row['filter']), true);
    } else {
      $stmt = $db->prepare('select name, email, atom, filter from users where user_id = :user_id');
      $stmt->execute($params);
      $row = $stmt->fetch();
      $db = null;
      if (empty($row['filter'])) $row['filter'] = '[""]';
      return array($row['name'], $row['email'], !$row['atom'], json_decode($row['filter']), true);
    }

  } catch(PDOException $e) {
    fail($e->getMessage());
  }
}

function get_or_set_tag($tag) {
  global $db, $debug;

  $params = array(
    ':tag' => $tag,
  );

  try {
    connect_sql();
    $stmt = $db->prepare('select tag_id from tags where tag = :tag');
    $stmt->execute($params);
    $count = $stmt->rowCount();
    if ($count == 0) {
      $stmt = $db->prepare('insert into tags (tag) values (:tag)');
      $stmt->execute($params);
      $id = $db->lastInsertId();
    } else {
      $row = $stmt->fetch();
      $id = $row['tag_id'];
    }
    $db = null;

  } catch(PDOException $e) {
    fail($e->getMessage());
  }

  return $id;
}

function get_tag($tag_id) {
  global $db, $debug;

  $params = array(
    ':tag_id' => $tag_id,
  );

  try {
    connect_sql();
    $stmt = $db->prepare('select tag from tags where tag_id = :tag_id');
    $stmt->execute($params);
    $count = $stmt->rowCount();
    $row = $stmt->fetch();
    $db = null;
    return $row['tag'];

  } catch(PDOException $e) {
    fail($e->getMessage());
  }
}

function get_tags($user_id) {
  global $db, $debug;

  $params = array(
    ':user_id' => pack('H*', $user_id),
  );

  try {
    connect_sql();

    $tags = array();
    $stmt = $db->prepare('select tag_id, tag from tags join tagmap using (tag_id) join commands using (command_id) where user_id = :user_id group by tag_id order by tag');
    $stmt->execute($params);
    $result = $stmt->fetchAll();
    $db = null;
    foreach ($result as $row) {
      $tags[] = array($row['tag_id'], $row['tag']);
    }

  } catch(PDOException $e) {
    fail($e->getMessage());
  }

  return $tags;
}

function get_command_tags($command_id) {
  global $db, $debug;

  $params = array(
    ':command_id' => pack('H*', $command_id),
  );

  try {
    connect_sql();

    $tags = array();
    $stmt = $db->prepare('select tag from tags join tagmap using (tag_id) join commands using (command_id) where command_id = :command_id order by tag desc');
    $stmt->execute($params);
    $result = $stmt->fetchAll();
    $db = null;
    foreach ($result as $row) {
      $tags[] = $row['tag'];
    }

  } catch(PDOException $e) {
    fail($e->getMessage());
  }

  return $tags;
}

function link_tag($tag_id, $command_id) {
  global $db, $debug;

  $params = array(
    ':tag_id' => $tag_id,
    ':command_id' =>  $command_id,
  );

  try {
    connect_sql();
    $stmt = $db->prepare('insert into tagmap (command_id, tag_id) values (:command_id, :tag_id)');
    $stmt->execute($params);
    $db = null;

  } catch(PDOException $e) {
    fail($e->getMessage());
  }
}

function pull_by_id($user_id) {
  global $db, $debug, $commands_per_page;

  $params = array(
    ':user_id' => pack('H*', $user_id),
    ':row_count' =>  $commands_per_page,
  );

  try {
    connect_sql();

    $commands = array();
    $stmt = $db->prepare('select command from commands where user_id = :user_id order by date desc limit :row_count');
    $stmt->execute($params);
    $result = $stmt->fetchAll();
    $db = null;
    foreach ($result as $row) {
      $commands[] = $row['command'];
    }

  } catch(PDOException $e) {
    fail($e->getMessage());
  }

  return array_reverse($commands);
}

function pull_by_tag($user_id, $tag) {
  global $db, $debug, $commands_per_page;

  $params = array(
    ':user_id' => pack('H*', $user_id),
    ':tag' => $tag,
    ':row_count' =>  $commands_per_page,
  );

  try {
    connect_sql();

    $commands = array();
    $stmt = $db->prepare('select command from commands join tagmap using (command_id) join tags using (tag_id) where user_id = :user_id and tag = :tag order by date desc limit :row_count');
    $stmt->execute($params);
    $result = $stmt->fetchAll();
    $db = null;
    foreach ($result as $row) {
      $commands[] = $row['command'];
    }

  } catch(PDOException $e) {
    fail($e->getMessage());
  }

  return array_reverse($commands);
}

function pull_by_keyword($user_id, $keyword) {
  global $db, $debug, $commands_per_page;

  $keyword = '%' . $keyword . '%';

  $params = array(
    ':user_id' => pack('H*', $user_id),
    ':keyword' => $keyword,
    ':row_count' =>  $commands_per_page,
  );

  try {
    connect_sql();

    $commands = array();
    $stmt = $db->prepare('select command from commands where user_id = :user_id and command like :keyword order by date desc limit :row_count');
    $stmt->execute($params);
    $result = $stmt->fetchAll();
    $db = null;
    foreach ($result as $row) {
      $commands[] = $row['command'];
    }

  } catch(PDOException $e) {
    fail($e->getMessage());
  }

  return array_reverse($commands);
}

function fetch_commands($user_id, $page, $atom=False) {
  global $db, $debug, $commands_per_page;

  $params = array(
    ':user_id' => pack('H*', $user_id),
    ':offset' => ($page - 1) * $commands_per_page,
    ':row_count' =>  $commands_per_page,
  );

  try {
    connect_sql();

    $commands = array();
    $stmt = $db->prepare('select command_id, date, command, status, annotation from commands where user_id = :user_id order by date desc limit :offset,:row_count');
    $stmt->execute($params);
    $result = $stmt->fetchAll();
    $db = null;
    foreach ($result as $row) {
      $id = bin2hex($row['command_id']);
      if ($atom) {
        $tags = array();
      } else {
        $tags = get_command_tags($id);
      }
      $commands[] = array(
        'date' => $row['date'],
        'id' => $id,
        'command' => $row['command'],
        'status' => $row['status'],
        'annotation' => $row['annotation'],
        'tags' => $tags
      );
    }

  } catch(PDOException $e) {
    fail($e->getMessage());
  }

  return $commands;
}

function fetch_commands_pages($user_id) {
  global $db, $debug, $commands_per_page;

  $params = array(
    ':user_id' => pack('H*', $user_id),
  );

  try {
    connect_sql();
    $stmt = $db->prepare('select count(command_id) from commands where user_id = :user_id');
    $stmt->execute($params);
    $command_count = $stmt->fetchColumn();
    $db = null;

  } catch(PDOException $e) {
    fail($e->getMessage());
  }

  return ceil($command_count/$commands_per_page);
}

function fetch_commands_by_filter($user_id, $filter, $page, $atom=False, $and=False) {
  global $db, $debug, $commands_per_page;

  if (is_array($filter)) {
    $params = $filter;
    $params[] = pack('H*', $user_id);
    $params[] = ($page - 1) * $commands_per_page;
    $params[] = $commands_per_page;

    $filterlength = implode(',', array_fill(0, count($filter), '?'));

    if ($and) {
      $sql = 'select command_id, date, command, status, annotation, count(*) as count from commands join tagmap using (command_id) join tags using (tag_id) where tag_id in ('.$filterlength.') and user_id = ? group by command_id having count = '.count($filter).' order by date desc limit ?,?';
    } else {
      $sql = 'select command_id, date, command, status, annotation from commands join tagmap using (command_id) join tags using (tag_id) where tag_id in ('.$filterlength.') and user_id = ? group by command_id order by date desc limit ?,?';
    }
  } else {
    $params = array(
      ':filter' => $filter,
      ':user_id' => pack('H*', $user_id),
      ':offset' => ($page - 1) * $commands_per_page,
      ':row_count' =>  $commands_per_page,
    );

    $sql = 'select command_id, date, command, status, annotation from commands join tagmap using (command_id) join tags using (tag_id) where tag = :filter and user_id = :user_id order by date desc limit :offset,:row_count';
  }

  try {
    connect_sql();

    $commands = array();
    $stmt = $db->prepare($sql);
    $stmt->execute($params);
    $result = $stmt->fetchAll();
    $db = null;
    foreach ($result as $row) {
      $id = bin2hex($row['command_id']);
      if ($atom) {
        $tags = array();
      } else {
        $tags = get_command_tags($id);
      }
      $commands[] = array(
        'date' => $row['date'],
        'id' => $id,
        'command' => $row['command'],
        'status' => $row['status'],
        'annotation' => $row['annotation'],
        'tags' => $tags
      );
    }

  } catch(PDOException $e) {
    fail($e->getMessage());
  }

  return $commands;
}

function fetch_commands_by_filter_pages($user_id, $filter) {
  global $db, $debug, $commands_per_page;

  $params = array(
    ':user_id' => pack('H*', $user_id),
    ':filter' => $filter,
  );

  try {
    connect_sql();

    $stmt = $db->prepare('select count(command_id) from commands join tagmap using (command_id) join tags using (tag_id) where tag = :filter and user_id = :user_id');
    $stmt->execute($params);
    $command_count = $stmt->fetchColumn();
    $db = null;

  } catch(PDOException $e) {
    fail($e->getMessage());
  }

  return ceil($command_count/$commands_per_page);
}

function fetch_command_by_id($user_id, $command_id) {
  global $db, $debug, $commands_per_page;

  $params = array(
    ':user_id' => pack('H*', $user_id),
    ':command_id' => pack('H*', $command_id),
  );

  try {
    connect_sql();

    $commands = array();
    $stmt = $db->prepare('select date, command, status, annotation from commands where user_id = :user_id and command_id = :command_id');
    $stmt->execute($params);
    $row = $stmt->fetch();
    $db = null;
    if (!empty($row)) {
      $tags = get_command_tags($command_id);
      $commands[] = array(
        'date' => $row['date'],
        'id' => $command_id,
        'command' => $row['command'],
        'status' => $row['status'],
        'annotation' => $row['annotation'],
        'tags' => $tags,
      );
    }
  } catch(PDOException $e) {
    fail($e->getMessage());
  }

  return $commands;
}

function fetch_commands_by_search($user_id, $query, $page) {
  global $db, $debug, $commands_per_page;

  $query = str_replace('*', '%', $query);
  $query = '%' . $query . '%';

  $params = array(
    ':user_id' => pack('H*', $user_id),
    ':query' => $query,
    ':offset' => ($page - 1) * $commands_per_page,
    ':row_count' =>  $commands_per_page,
  );

  try {
    connect_sql();

    $commands = array();
    $stmt = $db->prepare('select command_id, date, command, status, annotation from commands where user_id = :user_id and command like :query order by date desc limit :offset,:row_count');
    $stmt->execute($params);
    $result = $stmt->fetchAll();
    $db = null;
    foreach ($result as $row) {
      $id = bin2hex($row['command_id']);
      $tags = get_command_tags($id);
      $commands[] = array(
        'date' => $row['date'],
        'id' => $id,
        'command' => $row['command'],
        'status' => $row['status'],
        'annotation' => $row['annotation'],
        'tags' => $tags,
      );
    }

  } catch(PDOException $e) {
    fail($e->getMessage());
  }

  return $commands;
}

function fetch_commands_by_search_pages($user_id, $query) {
  global $db, $debug, $commands_per_page;

  $query = str_replace('*', '%', $query);
  $query = '%' . $query . '%';

  $params = array(
    ':user_id' => pack('H*', $user_id),
    ':query' => $query,
  );

  try {
    connect_sql();

    $stmt = $db->prepare('select count(command_id) from commands where user_id = :user_id and command like :query');
    $stmt->execute($params);
    $command_count = $stmt->fetchColumn();
    $db = null;

  } catch(PDOException $e) {
    fail($e->getMessage());
  }

  return ceil($command_count/$commands_per_page);
}

function set_atom_preference($user_id) {
  global $db, $debug, $max_filters;

  if (isset($_REQUEST['disable_atom'])) {
    $key = 'atom';
    $value = ($_REQUEST['disable_atom'] == 'true') ? 0 : 1;
  } elseif (isset($_REQUEST['filter'])) {
    $key = 'filter';
    $value= json_encode(array_map(create_function('$value', 'return (int)$value;'), array_slice($_REQUEST['filter'], 0, $max_filters)));
  } else {
    return;
  }

  $params = array(
    ':user_id' => pack('H*', $user_id),
    ':value' => $value,
  );

  if ($_SESSION['mainid'] != $_SESSION['id'] && $key == 'filter') {
    $table = 'identities';
    $user = 'alt_id';
  } else {
    $table = 'users';
    $user = 'user_id';
  }

  try {
    connect_sql();
    $stmt = $db->prepare('update ' . $table . ' set ' . $key . ' = :value where ' . $user . ' = :user_id');
    $stmt->execute($params);
    $db = null;

  } catch(PDOException $e) {
    fail($e->getMessage());
  }
}

function add_annotation($user_id, $command_id, $annotation) {
  global $db, $debug;

  if ($annotation == '') {
    $annotation = null;
  }

  $params = array(
    ':user_id' => pack('H*', $user_id),
    ':command_id' => pack('H*', $command_id),
    ':annotation' => $annotation,
  );

  try {
    connect_sql();
    $stmt = $db->prepare('update commands set annotation = :annotation where command_id = :command_id and user_id = :user_id');
    $stmt->execute($params);
    $db = null;

  } catch(PDOException $e) {
    fail($e->getMessage());
  }
}

function get_ids() {
  global $db, $debug;

  $ids = array();
  $ids[] = array($_SESSION['mainid'], 'Primary');

  $params = array(
    ':user_id' => pack('H*', $_SESSION['mainid']),
  );

  try {
    connect_sql();

    $stmt = $db->prepare('select alt_id, name from identities where user_id = :user_id order by created asc');
    $stmt->execute($params);
    $result = $stmt->fetchAll();
    $db = null;
    foreach ($result as $row) {
      $ids[] = array(bin2hex($row['alt_id']), $row['name']);
    }

  } catch(PDOException $e) {
    fail($e->getMessage());
  }
  return $ids;
}

function new_id() {
  global $db, $debug, $path, $max_user_ids;

  $ids = get_ids();

  if (count($ids) < $max_user_ids) {
    try {
      connect_sql();

      $params = array(
        ':user_id' => pack('H*', $_SESSION['mainid']),
      );
      $stmt = $db->prepare('update identities set active = 0 where user_id = :user_id');
      $stmt->execute($params);

      if (empty($_REQUEST['name'])) {
        $name = count($ids) + 1;
      } else {
        $name = $_REQUEST['name'];
      }
      $UUID = md5($name . microtime(true) . mt_rand(0, 0xffff));
      $params = array(
        ':user_id' => pack('H*', $_SESSION['mainid']),
        ':alt_id' => pack('H*', $UUID),
        ':name' => $name,
      );
      $stmt = $db->prepare('insert into identities (user_id, created, alt_id, active, name) values (:user_id, NOW(), :alt_id, 1, :name)');
      $stmt->execute($params);
      $db = null;
      $_SESSION['id'] = $UUID;
      $_SESSION['name'] = $name;
      if ($debug) debug_msg($_SESSION['mainid'] . ' created new identity ' . $UUID);
    } catch(PDOException $e) {
      fail($e->getMessage());
    }
  } else {
    if ($debug) debug_msg($_SESSION['mainid'] . ' tried to create too many identities');
  }

  $redirect_uri = '//' . $_SERVER['HTTP_HOST'] . $path . '/preferences';
  header('Location: ' . filter_var($redirect_uri, FILTER_SANITIZE_URL));
}

function change_id() {
  global $db, $debug, $path;

  $params = array(
    ':user_id' => pack('H*', $_SESSION['mainid']),
  );
  $params2 = array(
    ':user_id' => pack('H*', $_SESSION['mainid']),
    ':alt_id' => pack('H*', $_REQUEST['user_id']),
  );

  try {
    connect_sql();

    if ($_REQUEST['user_id'] == $_SESSION['mainid']) {
      $stmt = $db->prepare('update identities set active = 0 where user_id = :user_id');
      $stmt->execute($params);
      $_SESSION['id'] = $_SESSION['mainid'];
      $_SESSION['name'] = 'Primary';
      if ($debug) debug_msg($_SESSION['mainid'] . ' switched to Primary account');
    } else {
      $stmt = $db->prepare('select name from identities where user_id = :user_id and alt_id = :alt_id');
      $stmt->execute($params2);
      $result = $stmt->fetchAll();
      if (count($result) == 0) {
        if ($debug) debug_msg($_SESSION['mainid'] . ' failed to switch to account ' . $_REQUEST['user_id']);
      } else {

        $stmt = $db->prepare('update identities set active = 0 where user_id = :user_id');
        $stmt->execute($params);
        $stmt = $db->prepare('update identities set active = 1 where user_id = :user_id and alt_id = :alt_id');
        $stmt->execute($params2);

        $_SESSION['id'] = $_REQUEST['user_id'];
        $_SESSION['name'] = $result[0]['name'];

        if ($debug) debug_msg($_SESSION['mainid'] . ' switched to account ' . $_REQUEST['user_id']);
      }
    }

  } catch(PDOException $e) {
    fail($e->getMessage());
  }

  $redirect_uri = '//' . $_SERVER['HTTP_HOST'] . $path . '/preferences';
  header('Location: ' . filter_var($redirect_uri, FILTER_SANITIZE_URL));
}
