<?php

// http://stackoverflow.com/questions/7835752/how-to-do-page-navigation-for-many-many-pages-logarithmic-page-navigation#answer-14193775

// Used by paginationHTML below...
function paginationLink($p, $page, $URL) {
  if ($p==$page) return '<b style="color:#C0C0C0">' . $p . '</b>';
  return '<a href="' . $URL . $p . '">' . $p . '</a>';
}

// Used by paginationHTML below...
function paginationGap($p1, $p2) {
  $x = $p2-$p1;
  if ($x==0) return '';
  if ($x==1) return ' ';
  if ($x<=10) return ' . ';
  if ($x<=100) return ' .. ';
  return ' ... ';
}

// URL requires the $page number be appended to it.
// e.g. it should end in '&page=' or something similar.
function paginationHTML($page, $lastPage, $URL) {
  $LINKS_PER_STEP = 5;

  $result = '<div class="paging">'. PHP_EOL;
  // Nav buttons

  if ($page>1)
    $result .= '<a href="' . $URL . '1"><img src="img/first-arrow.png" alt="first"></a>&nbsp;'.
              '<a href="' . $URL . ($page-1) . '"><img src="img/lt-arrow.png" alt="prev"></a>';
  else $result .= '<img src="img/first-arrow-disabled.png" alt="first">&nbsp;<img src="img/lt-arrow-disabled.png" alt="prev">';

  $result .= '&nbsp;&nbsp;' . $page . '&nbsp;&nbsp;';
  if ($page<$lastPage)
    $result .= '<a href="' . $URL . ($page+1) . '"><img src="img/rt-arrow.png" alt="next"></a>&nbsp;'.
               '<a href="' . $URL . $lastPage . '"><img src="img/last-arrow.png" alt="last"></a>';
  else $result .= '<img src="img/rt-arrow-disabled.png" alt="next">&nbsp;<img src="img/last-arrow-disabled.png" alt="last">';
  $result .= "<br>";

  // Now calculate page links...

  $lastp1 = 1;
  $lastp2 = $page;
  $p1 = 1;
  $p2 = $page;
  $c1 = 1;
  $c2 = $LINKS_PER_STEP+1;
  $s1 = '';
  $s2 = '';
  $step = 1;
  while (true) {
    if ($c1>=$c2) {
      $s1 .= paginationGap($lastp1,$p1) . paginationLink($p1,$page,$URL);
      $lastp1 = $p1;
      $p1 += $step;
      $c1--;
    } else {
      $s2 = paginationLink($p2,$page,$URL) . paginationGap($p2,$lastp2) . $s2;
      $lastp2 = $p2;
      $p2 -= $step;
      $c2--;
    }
    if ($c2==0) {
      $step *= 10;
      $p1 += $step-1;         // Round UP to nearest multiple of $step
      $p1 -= ($p1 % $step);
      $p2 -= ($p2 % $step);   // Round DOWN to nearest multiple of $step
      $c1 = $LINKS_PER_STEP;
      $c2 = $LINKS_PER_STEP;
    }
    if ($p1>$p2) {
      $result .= $s1 . paginationGap($lastp1,$lastp2) . $s2;
      if (($lastp2>$page)||($page>=$lastPage)) break;
      $lastp1 = $page;
      $lastp2 = $lastPage;
      $p1 = $page+1;
      $p2 = $lastPage;
      $c1 = $LINKS_PER_STEP;
      $c2 = 1;
      $s1 = '';
      $s2 = '';
      $step = 1;
    }
  }
  $result .= '</div>' . PHP_EOL;
  return $result;
}

function paginationURL($url) {
  $url = preg_replace('/(?:&|(\?))page=[^&]*(?(1)&|)?/i', '$1', '?' . $url);
  if (parse_url($url, PHP_URL_QUERY)) {
    $url .= '&amp;page=';
  } else {
    $url = '?page=';
  }
  return $url;
}
?>
